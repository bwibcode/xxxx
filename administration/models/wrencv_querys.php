<?php

class WRENCV_create_queries
{
    private $wpdb_local;
    function __construct()
    {
        global $wpdb;
        $this->wpdb_local = $wpdb;
    }

    //registrar datos
    function wrencv_agregar_datos_bd($tabla, $datos)
    {
        $tabla = $this->wpdb_local->prefix . $tabla;
        return $this->wpdb_local->insert($tabla, $datos);
    }
    //Modificar regitros
    function wrencv_modificar_bd($tabla, $datos, $where)
    {
        $tabla = $this->wpdb_local->prefix . $tabla;
        return $this->wpdb_local->update($tabla, $datos, $where);;
    }
    //Eliminar registros
    function wrencv_eliminar_bd($tabla, $where)
    {
        $tabla = $this->wpdb_local->prefix . $tabla;
        return $this->wpdb_local->delete($tabla, $where);
    }
    //Maximo id
    function wrencv_maximo_id($tabla, $id)
    {
        $tabla = $this->wpdb_local->prefix . $tabla;
        $query = "SELECT id FROM $tabla ORDER BY id DESC limit 1";
        $resultado = $this->wpdb_local->get_results($query, ARRAY_A);
        return   $resultado[0]['id'];
    }
    // Buscar y cargar los datos depentdiendo de un dato especifico

    function wrencv_cargar_datos($tabla, $tabla1, $id_pregunta)
    {
        $tabla = $this->wpdb_local->prefix . $tabla; //ITEM
        $tabla1 = $this->wpdb_local->prefix . $tabla1; //PREGUNTAS
        $sql = "  SELECT $tabla.id, $tabla.descripcion_item, $tabla.id_pregunta, $tabla1.pregunta FROM  $tabla 
        INNER JOIN $tabla1 ON $tabla1.id = $tabla.id_pregunta and $tabla1.id_encuesta = '$id_pregunta' ORDER BY $tabla.id asc";
        return  $this->wpdb_local->get_results($sql);
    }

    //Cargar todos los datos
    function wrpor_load_encuestas($tabla)
    {
        $tabla = $this->wpdb_local->prefix . $tabla; //encuesta
        $sql = "SELECT *FROM $tabla";
        return  $this->wpdb_local->get_results($sql);
    }

    //Contar todos los bloques de preguntas existen
    function wrencv_count_bloques_item($tabla, $dato)
    {
        $tabla = $this->wpdb_local->prefix . $tabla; //items
        $sql = "SELECT count( $tabla.id_pregunta) AS cont_item from  $tabla where  $tabla.id_pregunta='$dato'";
        $resultado = $this->wpdb_local->get_results($sql);
        return $resultado != null ? $resultado[0]->cont_item : 0;
    }

    // Cargar las preguntas de la encuesta 


    



















    //Redireccionar paginas
    final function wrencv_admin_redireccionamiento($argumento = "")
    {
        ob_start();
        $url_redirect = (empty(admin_url()) ? network_admin_url($argumento) : admin_url($argumento));
        wp_redirect($url_redirect);
        exit;
    }
}
/*
SELECT wp_wrencv_items.id, wp_wrencv_items.descripcion_item, wp_wrencv_items.id_pregunta, 
wp_wrencv_question.pregunta FROM wp_wrencv_items 
INNER JOIN wp_wrencv_question ON wp_wrencv_question.id = wp_wrencv_items.id_pregunta 
and wp_wrencv_question.id_encuesta = 1 ORDER BY wp_wrencv_items.id asc*/