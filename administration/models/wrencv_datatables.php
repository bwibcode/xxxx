<?php

class WRENCV_Create_datatables
{
    /* private $wpdb_local;
    function __construct()
    {
        global $wpdb;
        $this->wpdb_local = $wpdb;
    }*/
    /**
     * titulo  de enceusta
     * 
     * 
     */
    public function wrencv_create_tables()
    {
        global $wpdb;
        //Titulo de la encuesta
        $sql_encuesta = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}wrencv_encuesta(
        `id` int NOT NULL AUTO_INCREMENT,
        `titulo` text NOT NULL,
        PRIMARY KEY (`id`));";
        $wpdb->query($sql_encuesta);
        //Preguntas de la encusta
        $sql_question = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}wrencv_question(
            `id` int(11) NOT NULL AUTO_INCREMENT,
           `id_encuesta` int(11) NOT NULL,
           `pregunta` text NOT NULL,
           FOREIGN KEY (`id_encuesta`) REFERENCES {$wpdb->prefix}wrencv_encuesta(`id`),
       PRIMARY KEY (`id`));";
        $wpdb->query($sql_question);
        //tabla items
        $sql_item = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}wrencv_items(
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `id_pregunta` int(11) NOT NULL,
        `descripcion_item` text NOT NULL,
        `is_selection` int(1) NOT NULL,
            FOREIGN KEY (`id_pregunta`) REFERENCES {$wpdb->prefix}wrencv_question(`id`),
            PRIMARY KEY (`id`));";
        $wpdb->query($sql_item);
        //tabla convinaciones preguntas
        $sql_conbinacion = "CREATE TABLE IF NOT EXISTS {$wpdb->prefix}wrencv_combinacion(
           `id` int(11) NOT NULL AUTO_INCREMENT,
           `id_encuesta` int(11) NOT NULL,
           `combinacion` text NOT NULL,
           `respuesta` text NOT NULL,
           FOREIGN KEY (`id_encuesta`) REFERENCES {$wpdb->prefix}wrencv_encuesta(`id`),
           PRIMARY KEY (`id`));";
        $wpdb->query($sql_conbinacion);
    }
}
