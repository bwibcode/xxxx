<?php


class WRENCV_Operaciones_combinacion extends WRENCV_create_queries
{
    function __construct()
    {
        parent::__construct();
    }

    function wrencv_add_combinacion()
    {
        $id_encuesta = ($_POST['id_encuesta']);
        $descripcion_combinacion = ($_POST['combinacion']);
        $respuesta_combinacion = ($_POST['res_combinacion']);
        $j = 0;
        try {
            while ($j < count($descripcion_combinacion)) {
                $datos = ['id_encuesta' => $id_encuesta[$j], 'combinacion' => $descripcion_combinacion[$j], 'respuesta' => $respuesta_combinacion[$j]];
                $this->wrencv_agregar_datos_bd("wrencv_combinacion", $datos);
                $j = $j + 1;
            }
            echo "registrado con exito";
            $this->wrencv_admin_redireccionamiento("admin.php?page=wrencv_menu");
        } catch (Exception $err) {
            echo "Campos requeridos " . $err;
            $this->wrencv_admin_redireccionamiento("admin.php?page=wrencv_menu");
        }
    }

    function wrencv_administrar_combinacion()
    {
        if (isset($_POST["crud"]) && $_POST["crud"] == "add") {
            $this->wrencv_add_combinacion();
        } else if (isset($_GET["crud"]) && $_GET["crud"] == "combinar") {

            $id_encuesta =  $_GET["id_encuesta"];

            $this->wrencv_admin_redireccionamiento("admin.php?page=wrencv_combinar_encuesta&id=$id_encuesta");

            //  $this->wrencv_convinar_items();
        } /*else if (isset($_POST["crud"]) && $_POST["crud"] == "update") {
        $this->wrpro_actualizar_proforma();
    } else if (isset($_POST["crud"]) && $_POST["crud"] == "remove") {
        $this->wrpro_eliminar_proforma();
    } else {
        $this->wrpro_admin_redireccionamiento("/admin.php?page=wrpro_menu_listproforma");
    }*/
    }
}
