<?php


class WRENCV_Operaciones_validar
{

    function wrencv_validaRequerido($valor)
    {
        if (trim($valor) == '') {
            return false;
        } else {
            return true;
        }
    }
    function wrencv_validarEntero($valor, $opciones = null)
    {

        if (filter_var($valor, FILTER_VALIDATE_FLOAT, $opciones) === FALSE) {
            return false;
        } else {
            return true;
        }
    }

    function wrencv_validarTelf($valor, $opciones = null)
    {
        if ((strlen($valor) <= 5 || strlen($valor) >= 12)) {
            return false;
        } else {
            return true;
        }
    }

    function wrencv_validaEmail($valor)
    {
        if (filter_var($valor, FILTER_VALIDATE_EMAIL) === FALSE) {
            return false;
        } else {
            return true;
        }
    }
}
