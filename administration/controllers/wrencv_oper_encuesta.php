<?php
class WRENCV_Operaciones_encuesta extends WRENCV_create_queries
{
    function __construct()
    {
        parent::__construct();
    }
    function wrencv_add_encuesta()
    {
        //Nombre de la encuesta
        $titulo_encuesta = sanitize_text_field($_POST['titulo_encuesta']);
        //Preguntas
        $descripcion_ = ($_POST['wr_descripcion']);
        $tipo = ($_POST['tipo']);
        $datos = ['id' => null, 'titulo' => $titulo_encuesta];
        if ($this->wrencv_agregar_datos_bd("wrencv_encuesta", $datos)) {
            //llamar al id de la encuesta ingresada
            $Id_encuesta = $this->wrencv_maximo_id("wrencv_encuesta", "id");
            $i = 0;
            while ($i < count($tipo)) {
                if ($tipo[$i] == "P") {
                    $datos_question = ['id' => null, 'id_encuesta' =>  $Id_encuesta, 'pregunta' => $descripcion_[$i]];
                    $this->wrencv_agregar_datos_bd("wrencv_question", $datos_question);
                    $Id_pregunta = $this->wrencv_maximo_id("wrencv_question", "id");
                }
                if ($tipo[$i] == "I") {
                    $datos_item = ['id' => null, 'id_pregunta' => $Id_pregunta, 'descripcion_item' => $descripcion_[$i], 'is_selection' =>  0];
                    $this->wrencv_agregar_datos_bd("wrencv_items", $datos_item);
                }
                $i = $i + 1;
            }
            $this->wrencv_admin_redireccionamiento("admin.php?page=wrencv_menu");
        }
    }

    function wrencv_administrar_encuesta()
    {
        if (isset($_POST["crud"]) && $_POST["crud"] == "add") {

            $this->wrencv_add_encuesta();
        } else if (isset($_GET["crud"]) && $_GET["crud"] == "combinar") {

            $id_encuesta =  $_GET["id_encuesta"];

            $this->wrencv_admin_redireccionamiento("admin.php?page=wrencv_combinar_encuesta&id=$id_encuesta");

            //  $this->wrencv_convinar_items();
        } /*else if (isset($_POST["crud"]) && $_POST["crud"] == "update") {
            $this->wrpro_actualizar_proforma();
        } else if (isset($_POST["crud"]) && $_POST["crud"] == "remove") {
            $this->wrpro_eliminar_proforma();
        } else {
            $this->wrpro_admin_redireccionamiento("/admin.php?page=wrpro_menu_listproforma");
        }*/
    }

    function wrenc_factorial($numero)
    {
        $factorial = 1;
        for ($i = $numero; $i >= 1; $i--) {
            $factorial = $factorial * $i;
        }

        return $factorial;
    }
    //obtener numero de combinaciones

    function wrenc_cantidad_combinaciones($n, $r)
    {
        // n cantidad de elementos  r numero de agrupamientos
        $cantidad = intdiv(intval($this->wrenc_factorial($n)), intval($this->wrenc_factorial($r)) * intval(($this->wrenc_factorial($n - $r))));
        // n! / r! (n-r)!
        return $cantidad;
    }

    //Contar cuantos grupos de preguntas ahi
    function wrencv_count_bloques($tabla, $tabla1, $id_encuesta)
    {
        $aux_temporal = 0;
        $cantidad_bloques = 0;
        $cargar_items =  $this->wrencv_cargar_datos($tabla, $tabla1, $id_encuesta);
        foreach ($cargar_items as $key => $value) {
            $id_pregunta = $value->id_pregunta;
            if ($id_pregunta != $aux_temporal) {
                $aux_temporal = $id_pregunta;
                $cantidad_bloques++;
            }
        }
        return $cantidad_bloques;
    }

    //Devolver los id item de las preguntas de una encuesta determinada
    function wrencv_array_item($tabla, $tabla1, $id_encuesta)
    {
        $array_num_item = array();
        $cargar_items =  $this->wrencv_cargar_datos($tabla, $tabla1, $id_encuesta);
        foreach ($cargar_items as $key => $value) {
            $id_item =  $value->id;
            $row_array['id_item'] = $id_item;
            array_push($array_num_item, $row_array);
        }
        return $array_num_item;
    }

    function wrencv_load_encuestas()
    {
        $tabla = "wrencv_encuesta";
        return $this->wrpor_load_encuestas($tabla);
    }

    // Cambiar id por item de las preguntas por la descripcion

    function wrencv_cambiar_question_description($id_encuesta)
    {
        $return_array_combinacion_ = array();
        // $id_encuesta = $_POST['id_encuesta'];
        $tabla = "wrencv_items";
        $tabla1 = "wrencv_question";
        $cantidad_bloques = $this->wrencv_count_bloques($tabla, $tabla1, $id_encuesta);
        $cargar_items =  $this->wrencv_cargar_datos($tabla, $tabla1, $id_encuesta);
        $cargar_combinaciones = $this->wrencv_cambiar_question_item($id_encuesta);
        $concatenar = "";
        $cont = 0;
        foreach ($cargar_combinaciones as $key => $value1) {
            $item_combinados = $value1['combinacion']; //biene la combinacion
            for ($u = 0; $u < strlen($item_combinados); $u++) {
                $digito =  $item_combinados[$u]; //b
                foreach ($cargar_items as $key => $value) {
                    $id_item = $key + 1;
                    $descripcion =  $value->descripcion_item;
                    if ($id_item > 9) {
                        $id_item = $this->wrencv_numeracion_codigos()[$id_item];
                    }
                    if ($digito == $id_item) {
                        $concatenar =   $concatenar . "+" . $descripcion;
                        $cont++;
                    }
                    if ($cont ==  $cantidad_bloques) {
                        $concatenar  = substr($concatenar, 1);
                        $row_array['descripcion'] = $concatenar;
                        array_push($return_array_combinacion_, $row_array);
                        $concatenar = "";
                        $cont = 0;
                    }
                }
            }
        }
        return $return_array_combinacion_;
    }

    //Contar la cantidad de digitos que posee un dato string
    function wrencv_validar_item($valor)
    {
        $cont = 0;
        for ($u = 0; $u < strlen($valor); $u++) {
            $cont++;
        }
        return $cont;
    }
    // Combinacion de items
    function wrencv_cambiar_question_item($id_encuesta)
    {
        $array_numeros = array();
        $array_ultima_columna = array();
        $tabla = "wrencv_items";
        $tabla1 = "wrencv_question";
        $cantidad_bloques = $this->wrencv_count_bloques($tabla, $tabla1, $id_encuesta);
        $cargar_items =  $this->wrencv_cargar_datos($tabla, $tabla1, $id_encuesta);
        $registrar_items_combinaciones = array();
        $cont = 0;
        //  echo count($cargar_items);
        foreach ($cargar_items as $key => $value) { //hasta ahora debe ser menor o igual que 9 <=60
            $cont++;
            if ($cont > 9) {
                $cont = $this->wrencv_numeracion_codigos()[$cont];
            }
            $array_numeros[$key] = $cont;
        }
        $cont_ultimo = 0;

        $array_ultimo_auxiliar = array();
        $array_ultimo_auxiliar = $this->wrencv_array_ultima_columna($id_encuesta);

        $array_primero_auxiliar = array();
        $array_primero_auxiliar =  $this->wrencv_array_primera_columna($id_encuesta);

        $elementos = $array_numeros; //  array('1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'f', 'g', 'h', 'i', 'j', 'M');
        $elementos_invert = array_flip($elementos);
        $cuantos = count($elementos);
        $corridas = array();
        $resultados = $elementos;
        // Por cada elemento, hay una vuelta, un nCr
        for ($i = 1; $i <= $cuantos; $i++) {
            // La primera corrida son los elementos a secas
            if (!isset($corridas[$i - 1])) {
                $corridas[$i] = $elementos;
                continue;
            }
            $elementos_new = $elementos;
            // Las letras sobrantes
            $letras_slice = array_slice($elementos_new, ($i - 1));
            $letras_rebuilt = implode("", $letras_slice);
            // Cada elemento de la vuelta anterior es una vuelta en esta
            foreach ($corridas[$i - 1] as $j => $itera) {
                $subletras = str_split($itera);
                $subletras_c = count($subletras) - 1;
                // Cada letra de las letras sobrantes es otra vuelta
                foreach ($letras_slice as $itera2) {
                    $letra_fin = $elementos_invert[$subletras[$subletras_c]];
                    $letra_coteja = $elementos_invert[$itera2];
                    if (
                        !in_array($itera2, $subletras) && ($letra_fin < $letra_coteja)
                    ) {
                        $corridas[$i][] = $itera . $itera2;
                        $dato_combinado = $itera . $itera2;
                        if ($this->wrencv_validar_item($dato_combinado) ==  $cantidad_bloques) {
                            $cont_first = 1;
                            $cont_ultimo = 1;

                            for ($s = 0; $s < count($array_primero_auxiliar); $s++) { //1 2 3 4
                                $primer_dato =  $array_primero_auxiliar[$s]; //1  2 
                                for ($m = 0; $m < strlen($dato_combinado); $m++) { //458                                         
                                    $digito = $dato_combinado[$m]; //1
                                    if ($primer_dato == $digito) {
                                        $cont_first++; //2
                                    }
                                }
                            }


                            if ($cont_first == 2) {

                                for ($a = 0; $a <= count($array_ultimo_auxiliar); $a++) {
                                    $ultimo_dato =  $array_ultimo_auxiliar[$a]; //1 2 3 4 
                                    for ($u = 0; $u < strlen($dato_combinado); $u++) {
                                        $digito =    $dato_combinado[$u];
                                        if ($ultimo_dato == $digito) {
                                            $cont_ultimo++;
                                        }
                                    }
                                }
                                if ($cont_ultimo == 2) {

                                    $ultimo =  substr($dato_combinado, -1); //se obtiene el ultimo digito
                                    for ($j = 0; $j <= count($array_ultimo_auxiliar); $j++) {
                                        if ($ultimo == $array_ultimo_auxiliar[$j]) {

                                            $row_array['combinacion'] =   $dato_combinado;
                                            array_push($registrar_items_combinaciones, $row_array);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $registrar_items_combinaciones;
    }
    //Cargar los datos de la primera columna
    function wrencv_array_primera_columna($id_encuesta)
    {
        $numero = 0;
        $tabla = "wrencv_items";
        $tabla1 = "wrencv_question";
        $cargar_items =  $this->wrencv_cargar_datos($tabla, $tabla1, $id_encuesta);
        $array_primero_auxiliar = array();
        $bandera = true;
        foreach ($cargar_items as $key => $value) {
            if ($bandera) {
                $count_cantidad_question = $this->wrencv_count_bloques_item($tabla, $value->id_pregunta);
                $bandera = false;
            }
            for ($i = 0; $i < $count_cantidad_question; $i++) {

                $numero = $i + 1;
                if ($numero > 9) {
                    $numero = $this->wrencv_numeracion_codigos()[$numero];
                }
                $array_primero_auxiliar[$i] = $numero;
            }
        }
        return  $array_primero_auxiliar;
    }


    //Registra todo los items de la ultima columna
    function wrencv_array_ultima_columna($id_encuesta)
    {
        $aux_contador = 0;
        $aux_temporal = 0;
        $cont_ultimo = 0;
        $numero = 0;
        $tabla = "wrencv_items";
        $tabla1 = "wrencv_question";
        $cantidad_bloques = $this->wrencv_count_bloques($tabla, $tabla1, $id_encuesta);
        $cargar_items =  $this->wrencv_cargar_datos($tabla, $tabla1, $id_encuesta);
        $array_ultimo_auxiliar = array();
        foreach ($cargar_items as $key => $value) {
            $id_pregunta  = $value->id_pregunta;
            if ($id_pregunta != $aux_temporal) {
                $aux_temporal = $id_pregunta;
                $aux_contador++;
            }
            if ($aux_contador >= $cantidad_bloques) {

                $numero = $key + 1;
                if ($numero > 9) {
                    $numero = $this->wrencv_numeracion_codigos()[$numero];
                }

                $array_ultimo_auxiliar[$cont_ultimo] =  $numero; // 6
                $cont_ultimo++;
            }
        }
        return  $array_ultimo_auxiliar;
    }
    //Array de letras asignadas un index mayor a 9
    function wrencv_numeracion_codigos()
    {
        $array_codigos = array(10 => 'a', 11 => 'b', 12 => 'c', 13 => 'd', 14 => 'e', 15 => 'f', 16 => 'g', 17 => 'h', 18 => 'i', 19 => 'j', 20 => 'k', 21 => 'l', 22 => 'm', 23 => 'n', 24 => 'o', 25 => 'p', 26 => 'q', 27 => 'r', 28 => 's', 29 => 't', 30 => 'u', 31 => 'v', 32 => 'w', 33 => 'x', 34 => 'y', 35 => 'z', 36 => 'A', 37 => 'B', 38 => 'C', 39 => 'D', 40 => 'E', 41 => 'F', 42 => 'G', 43 => 'H', 44 => 'I', 45 => 'J', 46 => 'K', 47 => 'L', 48 => 'M', 49 => 'N', 50 => 'O', 51 => 'P', 52 => 'Q', 53 => 'R', 54 => 'S', 55 => 'T', 56 => 'U', 57 => 'V', 58 => 'W', 59 => 'X', 60 => 'Y', 61 => 'Z');
        return  $array_codigos;
    }
}
