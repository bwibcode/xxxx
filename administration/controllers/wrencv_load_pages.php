<?php

class WRENCV_AdminLoad_PageController
{
    function __construct()
    {
    }
    final function wrencv_load_page($page)
    {
        if ($page == "inicio.phtml") {
            wp_enqueue_style(
                'wrencv_encuesta',
                plugin_dir_url(__FILE__) . '../../static/css/style_encuesta.css',
                array(),
                "1.0.0"
            );


        } else if ($page == "encuesta.phtml") {


            wp_enqueue_style(
                'wrencv_encuesta',
                plugin_dir_url(__FILE__) . '../../static/css/style_encuesta.css',
                array(),
                "1.0.0"
            );

            wp_enqueue_script('wrencv_js_encuesta', plugins_url('../../static/js/wrencv_encuesta.js', __FILE__), array('jquery'));
      
        }else if($page == "combinar.phtml"){
            wp_enqueue_style(
                'wrencv_encuesta',
                plugin_dir_url(__FILE__) . '../../static/css/style_encuesta.css',
                array(),
                "1.0.0"
            );

        }
        require_once plugin_dir_path(__FILE__) . '../views/' . $page;
    }
}
