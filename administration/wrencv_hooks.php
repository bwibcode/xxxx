<?php

add_action('admin_menu', 'wrencv_menu_admin');
function wrencv_menu_admin()
{
    add_menu_page(
        'Encuesta',
        'Encuesta',
        'manage_options',
        'wrencv_menu',
        'wrencv_subemnu_showprincipal',
        plugin_dir_url(__FILE__) . '../static/imagenes/encuesta.png',
        '2'
    );
    add_submenu_page('wrencv_menu', 'crearencuesta', 'Crear Encuesta', 'manage_options', 'wrencv_create_encuesta', 'wrencv_submenu_createncuesta');
    add_submenu_page('wrencv_menu', 'combinarencuesta', 'Combinar Encuesta', 'manage_options', 'wrencv_combinar_encuesta', 'wrencv_submenu_combinarencuestas');
}

function wrencv_subemnu_showprincipal() //Muestra encuestas
{
    $wrpro = new WRENCV_AdminLoad_PageController();
    $wrpro->wrencv_load_page("inicio.phtml");
}

function wrencv_submenu_createncuesta() //Muestra encuestas
{
    $wrpro = new WRENCV_AdminLoad_PageController();
    $wrpro->wrencv_load_page("encuesta.phtml");
}

function wrencv_submenu_combinarencuestas() //Muestra encuestas
{
    $wrpro = new WRENCV_AdminLoad_PageController();
    $wrpro->wrencv_load_page("combinar.phtml");
}

add_action('admin_post_wrencv-seleccionar-opcion', 'wrencv_opciones_encuesta');
function wrencv_opciones_encuesta()
{
    $encuesta = new WRENCV_Operaciones_encuesta;
    $encuesta->wrencv_administrar_encuesta();
}

add_action('admin_post_wrencv-seleccionar-opcion-combinacion', 'wrencv_opciones_combinaciones');
function wrencv_opciones_combinaciones()
{
    $combinacion = new WRENCV_Operaciones_combinacion();
    $combinacion->wrencv_administrar_combinacion();
}
