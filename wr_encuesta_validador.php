<?php
/**
 * Plugin Name: WR encuesta_validador
 * Plugin URI: https://www.webrevolutionagency.com/
 * Description: Plugin que permite crear encuestas y validar respuestas.
 * Version: 1.0.0
 * Author: Web Revolution Milano
 * Author https://www.webrevolutionagency.com/
 * License: GPL2
 * Text Domain: wr-encuesta-validador
 * Domain Path: /languages
 */

defined('ABSPATH') or die('');

require_once dirname(__FILE__) . '/shortcode/administration/wrencv_load_pages.php';

require_once dirname(__FILE__) . '/administration/models/wrencv_querys.php'; 
require_once dirname(__FILE__) . '/shortcode/wrencv_hook.php';
require_once dirname(__FILE__) . '/shortcode/administration/wrencv_oper_encuesta.php';



require_once dirname(__FILE__) . '/administration/controllers/wrencv_oper_encuesta.php'; 

if (is_admin()) {
    require_once dirname(__FILE__) . '/administration/models/wrencv_datatables.php';
    require_once dirname(__FILE__) . '/administration/models/wrencv_querys.php';  
    require_once dirname(__FILE__) . '/administration/wrencv_hooks.php'; 
    require_once dirname(__FILE__) . '/administration/controllers/wrencv_load_pages.php'; 
    require_once dirname(__FILE__) . '/administration/controllers/wrencv_oper_encuesta.php'; 
    require_once dirname(__FILE__) . '/administration/controllers/wrencv_oper_combinacion.php'; 
    require_once dirname(__FILE__) . '/administration/controllers/wrencv_validar_campos.php'; 
    


}

register_activation_hook(__FILE__, 'wrencv_activar');

function wrencv_activar()
{
    $datatables = new WRENCV_Create_datatables;
    $datatables->wrencv_create_tables();
}